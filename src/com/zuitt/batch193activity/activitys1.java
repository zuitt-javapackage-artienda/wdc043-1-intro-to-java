package com.zuitt.batch193activity;

import java.util.Scanner;

public class activitys1 {

    public static void main(String[]args){
        Scanner appScanner = new Scanner(System.in);
        System.out.println("What's your First Name?");
        String firstName = appScanner.nextLine().trim();

        System.out.println("What's your Last Name?");
        String lastName = appScanner.nextLine().trim();

        System.out.println("What's First Subject Grade?");
        double firstGrade = appScanner.nextDouble();

        System.out.println("What's Second Subject Grade?");
        double secondGrade = appScanner.nextDouble();

        System.out.println("What's Third Subject Grade?");
        double thirdGrade = appScanner.nextDouble();

        double totalGrade = firstGrade + secondGrade + thirdGrade;

        double aveGrade = totalGrade/3;
        int aveGradeInt = (int)aveGrade;

        System.out.println("Good day " + firstName + " "+ lastName + "." + " " + "Your average grade is: " + aveGradeInt);
    }

}
